# emacs

Configuration and scripts for emacs so that life is easier when jumping onto a new machine

installation is by cloning the repo and symlinking .emacs to emacs/dot_emacs

```
cd ~
git clone git@gitlab.com:timdrysdale/emacs.git
ln -s ~/emacs/dot_emacs ./.emacs
```

The load path is automatically adjusted to include the `emacs/lisp` directory.

use MELPA to install use-package
M-x list-packages
(navigate to use-package and install)

